
// base dependencies for app
var express = require('express');
var passport = require('passport');
var mongoose = require('mongoose');
var bodyParser=require('body-parser');

var db = mongoose.connect("mongodb://localhost/googleUser");
//   , mongoStore = require('connect-mongodb')
var http = require('http');
var path = require('path');
var GoogleStrategy = require('passport-google-oauth2').Strategy;




var app = express();

// Make our db accessible to our router
app.use(function(req,res,next){
    req.db = db;
    next();
});

app.use(bodyParser.json());                                     
app.use(bodyParser.urlencoded({extended: true}));               // parse application/x-www-form-urlencoded
app.use(bodyParser.text());                                     // allows bodyParser to look at raw text
app.use(bodyParser.json({ type: 'application/vnd.api+json'}));  

passport.use(new GoogleStrategy({
    clientID:     '706981438981-40ico2fbi6eqmeqsrhbpgnmi54siblol.apps.googleusercontent.com',
    clientSecret: 'NmwgP_HBWTPsAL6dt2Zc6Fwp',
    callbackURL: "https://lwei47-ece9065-lab3-lwei47.c9users.io/auth/google/callback",
    passReqToCallback   : true
  },
  function(request, accessToken, refreshToken, profile, done) {
    User.findOrCreate({ googleId: profile.id }, function (err, user) {
      return done(err, user);
    });
  }
));

var serverPath = __dirname.split(path.sep);
var client_dirname = path.join('/', serverPath[1], serverPath[2], serverPath[3], 'client');

app.use(express.static(path.join(client_dirname, 'public')));
app.use(express.static(path.join(client_dirname, 'bower_components')));
app.use(express.static(path.join(client_dirname, 'Views')));
//app.use(express.static(path.join(__dirname, 'public')));
// app.use(express.static(path.join(__dirname, 'bower_components')));
// app.use(express.static(path.join(__dirname, 'Views')));

app.set('views', path.join(client_dirname, 'Views'));


app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);


require('./config/routes.js')(app, passport);


var server = http.createServer(app);


server.listen(process.env.PORT, process.env.IP, function(){
  console.log("Server listening on: http://localhost:%s", process.env.PORT);
});

 