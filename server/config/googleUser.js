'use strict';
// Pulls Mongoose dependency for creating schemas
var mongoose=require('mongoose');
var Schema=mongoose.Schema;

// Creates a User Schema. This will be the basis of how user data is stored in the db
var UserSchema = new Schema({
    id              :{type:String},
    name            : {type: String},
    email           : {type: String},
    created_at      : {type: Date, default: Date.now},
    updated_at      : {type: Date, default: Date.now},
    posts:          [{
                        thread      : {type:String},
                        comment     : [{
                                entity      : {type: String},
                                comment     : {type: String}
                            }]
                    }]
});

UserSchema.pre('save', function(next){
    var now = new Date();
    this.updated_at = now;
    if(!this.created_at) {
        this.created_at = now;
    }
    next();
});

// Indexes this schema in 2dsphere format (critical for running proximity searches)
UserSchema.index({location: '2dsphere'});


module.exports = mongoose.model('googleUser', UserSchema);
