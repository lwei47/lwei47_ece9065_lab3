/**
 * Main application routes
 */

'use strict';

var qs              = require("qs")
var request         = require("request")
var mongoose        = require('mongoose');
var User            = require('./googleUser.js');
var bodyParser      = require('body-parser');
var morgan          = require('morgan');
var methodOverride  = require('method-override');

module.exports = function(app, passport) {

    var callbackURL = "https://lwei47-ece9065-lab3-lwei47.c9users.io/callback";
    var CLIENT_ID = '706981438981-40ico2fbi6eqmeqsrhbpgnmi54siblol.apps.googleusercontent.com';
    var CLIENT_SECRET = 'NmwgP_HBWTPsAL6dt2Zc6Fwp';
    var state = 'lwei47';
    var access_token = '';
    var token_type = '';
    var expires = '';
    
    app.get('/test', function(req, res) {
        res.json({ message: 'hooray! welcome to our api!' });
        console.log('test pass');
    });
    
    
    app.get("/login", function(req, res) {
  

        // Generate a unique number that will be used to check if any hijacking
        // was performed during the OAuth flow
        state = "lwei47";
        
        var params = {
            response_type: "code",
            client_id: CLIENT_ID,
            redirect_uri: callbackURL,
            state: state,
            display: "popup",
            scope: "profile email"
            //scope: "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email"
        };
        
        console.log("user loged in");
        params = qs.stringify(params);
        console.log("login param:" + params);
        //res.writeHead(200, {'Content-Type': 'text/plain'});
        res.redirect("https://accounts.google.com/o/oauth2/auth?" + params);
    });
    
    app.get("/callback", function(req, res) {
  
        // Collect the data contained in the querystring
        var code = req.query.code
          , cb_state = "lwei47"
          , error = req.query.error;
        console.log("callback state:" + state);
        console.log("callback cb_state:" + cb_state);
        // Verify the 'state' variable generated during '/login' equals what was passed back
        if (state == cb_state) {
            if (code !== undefined) {
              
                // Setup params and URL used to call API to obtain an access_token
                var params = {
                    code: code,
                    client_id: CLIENT_ID,
                    client_secret: CLIENT_SECRET,
                    redirect_uri: callbackURL,
                    grant_type: "authorization_code"
                };
                var url = "https://accounts.google.com/o/oauth2/token";
                
                // Send the API request
                request.post(url, {form: params}, function(err, resp, body) {
                  
                    // Handle any errors that may occur
                    if (err) return console.error("Error occured: ", err);
                    var results = JSON.parse(body);
                    if (results.error) return console.error("Error returned from Google: ", results.error);
                    
                    // Retrieve and store access_token to session
                    access_token = results.access_token;
                    token_type = results.token_type;
                    expires = results.expires_in;
                    
                    console.log("Connected to Google");
                    console.log(access_token);
                    console.log(token_type);
                    console.log(expires);
                    
                    
                    // Close the popup authentication. This will trigger the client (index.html) to redirect

                    var output = '<html><head></head><body onload="window.close();">Close this window</body></html>';
                    res.writeHead(200, {'Content-Type': 'text/html'});
                    //res.redirect("/#/posting");
                    
                    res.end(output);
                });
            } else {
                console.log("Code is undefined: " + code);
                console.log("Error: " + error);
            }
        } else {
            console.log('Mismatch with variable "state". Redirecting to /');
            res.redirect("/");
        }
    });
    
    // Get access_token and fetch user profile
    app.get("/user", function(req, res) {
  
        // Check to see if user as an access_token first
        if (access_token) {
          
            // URL endpoint and params needed to make the API call  
            var url = "https://www.googleapis.com/oauth2/v1/userinfo";
            var params = {
                access_token: access_token
            };
    
            // Send the request
            request.get({url: url, qs: params}, function(err, resp, user) {
                // Check for errors
                if (err) return console.error("Error occured: ", err);
                
                // Send output as response
                var userinfo=JSON.parse(user);
                var newuser = new User();
                
                newuser.id = userinfo.id;
                newuser.email = userinfo.email;
                newuser.name = userinfo.name;
                console.log(JSON.stringify(userinfo));
                console.log(newuser);
                User.findOne({'id':userinfo.id}, function(err, User) {
                    if (err)
                        newuser.save(function(err){
                        if(err)
                            res.json(err);
                         });
                    
                });
                // newuser.save(function(err){
                //     if(err)
                //     res.json(err);
                //      });
                res.end(user);
                
            });
        } else {
            console.log("Couldn't verify user was authenticated. Redirecting to /");
            res.redirect("/");
        }
    });
    
    
    // get the user with id (accessed at GET http://localhost:8080/user/:id)
    app.get('/user/:id',function(req, res) {
        User.findOne({'id':userinfo.id}, function(err, User) {
            if (err)
                res.send(err);
            res.json(User);
        });
    });
    //Initiate data frame for user
    app.post('/user',function(req, res) {
        var newuser = new User(req.body);
        newuser.id = req.body.id;
        newuser.name = req.body.name;
        //console.log(JSON.stringify(req.body.name));
        newuser.email = req.body.email;
        //newuser.posts.push({ comment: 'Nothing to say' });
        newuser.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'profile created!' });
        });
        
    });
    
    app.get('/post', function(req, res) {
    
        User.find(function(err,user){
           if(err)
            {return res.send(err);}
        
        res.send(user);
    });
        
    });
    //Create post for logged-in user
    app.post('/post', function(req, res) {
	   var user=new User();
            user.posts.push({thread : req.body.text});
            user.save(function(err){
                if(err)
                 {return res.send(err);}
                 
                res.send({message: 'posts created!'})
            });
    });
    //Update post for logged-in user
    app.put('/post', function(req, res) {
        console.log("User's email:" + req.body.email);
        User.findOne({'email': req.body.email}, function(err, user) {
            if (err){
                res.send(err);
            }else{
                console.log("Posts are coming!");
                //console.log(user.posts);
                res.send(user.posts);
            }
        });
    });

    
	 //Create comment by email
	 app.post('/post/:email', function(req, res) {
	   User.findOne({'email':req.params.email}, function(err, user) {
	   var user=new User();
	        user.email = req.params.email;
            user.posts.push({thread : req.body.text});
            user.save(function(err){
                if(err)
                 {return res.send(err);}
                 
                res.send({message: 'posts created!'})
            });
	   });

	});
	//Update post by email
	app.put('/post/:email', function(req, res) {
	   User.findOne({'email':req.params.email}, function(err, user) {
	   var user=new User();
	        user.email = req.params.email;
            user.posts.push({thread : req.body.text});
            user.save(function(err){
                if(err)
                 {return res.send(err);}
                 
                res.send({message: 'posts updated!'})
            });
	   });

	});
	
	//Delete post by email
	app.delete('/post/:email', function(req, res) {
	   console.log(req.body);
        User.findOneAndRemove({'email':req.params.email}, function (err, user) {
            console.log(user.email);
            //if (!err) {
            //var user=new User();
            var post_num = user.posts.length;
            console.log(post_num);
            res.send("Deleted successfully!");
            // user.posts[post_num-1].remove();
            // user.save(function (err) {
            //     if(err){
            //         res.send(err);
            //     }
                
            //     res.send("Deleted successfully!");
                
            // });
            
        });

	});
	//Get comment by email
	app.get('/post/comment/:email',function(req, res) {
        User.findOne({'email':req.params.email}, function(err, User) {
            if (err)
                res.send(err);
            res.json(User);
        });
    });
	//Create comment by email
	app.post('/post/comment/:email', function(req, res) {
	   User.findOne({'email':req.params.email}, function(err, user) {
	   var user=new User();
	        user.email = req.params.email;
            user.posts.push({comment : req.body.text});
            user.save(function(err){
                if(err)
                 {return res.send(err);}
                 
                res.send({message: 'Comments created!'})
            });
	   });

	});
	//Update comment by email
	app.put('/post/comment/:email', function(req, res) {
	   User.findOne({'email':req.params.email}, function(err, user) {
	   var user=new User();
	        user.email = req.params.email;
            user.posts.push({thread : req.body.text});
            user.save(function(err){
                if(err)
                 {return res.send(err);}
                 
                res.send({message: 'Comments updated!'})
            });
	   });

	});
	
	//Delete comment by email
	app.delete('/post/comment/:email', function(req, res) {
	   console.log(req.body);
        User.findOneAndRemove({'email':req.params.email}, function (err, user) {
            console.log(user.email);
            //if (!err) {
            //var user=new User();
            var post_num = user.posts.length;
            console.log(post_num);
            res.send("Deleted successfully!");
            
            
        });

	});
    
    
    
    
    app.get('/auth/google/token', passport.authenticate('google-token'),
     function(req, res) {
      res.send(req.user);
      console.log('get user');
    });
    // =====================================
    // GOOGLE ROUTES =======================
    // =====================================
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their emails
    app.get('/auth/google',
	  passport.authenticate(
	  	'google',
		  {
		  	scope: [
		  	'https://www.googleapis.com/auth/userinfo.profile',
		  	'https://www.googleapis.com/auth/userinfo.email'
		  	]
		  })
	  );

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
            passport.authenticate('google', {
                    scope: [
        		  	'https://www.googleapis.com/auth/userinfo.profile',
        		  	'https://www.googleapis.com/auth/userinfo.email'
        		  	],
                    successRedirect : '/posting',
                    failureRedirect : '/'
            }));
            
    
    // route for home page
    app.get('/', function(req, res) {
        res.render('Index.html'); // load the Index.html file
    });
    
    app.get('/posting', isLoggedIn, function(req, res) {
        res.render('Posting.html', {
            user : req.user // get the user out of session and pass to template
        });
    });
    

    // route for logging out
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });



    

};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}