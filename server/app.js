/**
 * Main application file
 */

'use strict';

var express = require('express');
var passport = require('passport');
var mongoose = require('mongoose');
//   , mongoStore = require('connect-mongodb')
var http = require('http');
var config = require('./config/environment');
var GoogleStrategy = require('passport-google-oauth2').Strategy;

var app = express();

passport.use(new GoogleStrategy({
    clientID:     '706981438981-40ico2fbi6eqmeqsrhbpgnmi54siblol.apps.googleusercontent.com',
    clientSecret: 'NmwgP_HBWTPsAL6dt2Zc6Fwp',
    callbackURL: "https://lwei47-ece9065-lab3-lwei47.c9users.io/auth/google/callback",
    passReqToCallback   : true
  },
  function(request, accessToken, refreshToken, profile, done) {
    User.findOrCreate({ googleId: profile.id }, function (err, user) {
      return done(err, user);
    });
  }
));

// Connect to MongoDB
// mongoose.connect(config.mongo.uri, config.mongo.options);
// mongoose.connection.on('error', function(err) {
//   console.error('MongoDB connection error: ' + err);
//   process.exit(-1);
// });

// Populate databases with sample data
// if (config.seedDB) { require('./config/seed'); }

// Setup server
var app = express();
var server = http.createServer(app);
require('./config/express')(app);
require('./routes')(app);

// Start server
function startServer() {
  app.angularFullstack = server.listen(config.port, config.ip, function() {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
  });
}

setImmediate(startServer);

// Expose app
exports = module.exports = app;
