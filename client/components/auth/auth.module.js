'use strict';

angular.module('lwei47Ece9065Lab2App.auth', [
  'lwei47Ece9065Lab2App.constants',
  'lwei47Ece9065Lab2App.util',
  'ngCookies',
  'ngRoute'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
