/**
 * Main application routes
 */

'use strict';
var User = require('./googleUser.js');

module.exports = function(app, passport) {

    app.get('/auth/google/token', passport.authenticate('google-token'),
     function(req, res) {
      res.send(req.user);
    });
    // =====================================
    // GOOGLE ROUTES =======================
    // =====================================
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their emails
    app.get('/auth/google',
	  passport.authenticate(
	  	'google',
		  {
		  	scope: [
		  	'https://www.googleapis.com/auth/userinfo.profile',
		  	'https://www.googleapis.com/auth/userinfo.email'
		  	]
		  })
	  );

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
            passport.authenticate('google', {
                    scope: [
        		  	'https://www.googleapis.com/auth/userinfo.profile',
        		  	'https://www.googleapis.com/auth/userinfo.email'
        		  	],
                    successRedirect : '/posting',
                    failureRedirect : '/'
            }));
            
    
    // route for home page
    app.get('/', function(req, res) {
        res.render('Index.html'); // load the Index.html file
    });
    
    app.get('/posting', isLoggedIn, function(req, res) {
        res.render('Posting.html', {
            user : req.user // get the user out of session and pass to template
        });
    });
    

    // route for logging out
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });



    

};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}