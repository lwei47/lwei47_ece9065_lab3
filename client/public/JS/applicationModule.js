var myApp = angular.module('myApp', ['ngAnimate', 'ui.bootstrap', 'ngRoute', 'myModule']);
//-------- create the view module and name-----------//

    // configure four routes
    myApp.config(function($routeProvider) {
        $routeProvider

            // route for the login page
            .when('/', {
                templateUrl : 'Login.html',
                controller  : 'loginController'
            })
            
            // Inject to login page
            .when('/login', {
                templateUrl : 'Login.html',
                controller  : 'loginController'
            })

            // Inject to posting page
            .when('/posting', {
                templateUrl : 'Posting.html',
                controller  : 'postingController'
            })

            // rInject to posts tracking page
            .when('/tracking', {
                templateUrl : 'PostsTracking.html',
                controller  : 'trackingController'
            })
            
            // rInject to post comments page
            .when('/comments', {
                templateUrl : 'SpecificPostComments.html',
                controller  : 'commentsController'
            })
    });