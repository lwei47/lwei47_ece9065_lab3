myApp.controller('loginController', function ($scope, $log) {
  
  $scope.status = {
    isopen: false
  };
		
	$location.path("/posting");

  $scope.toggled = function(open) {
    $log.log('Dropdown is now: ', open);
  };

  $scope.toggleDropdown = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };

  $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
  

});

// create the controller and inject Angular's $scope
myApp.controller('postingController', function ($scope, $http) {
   
    $scope.isLogin = false;
    $scope.status = {
    isopen: false
  };
  
  $scope.posts = [];
  $scope.text = '';
  $scope.postCount = 0;
  
  $scope.post = {
        text: '',
        word: /^\s*\w*\s*$/
      };
  $scope.posts = $scope.post.text;
  
  $scope.PostCreate = function(){
        
      //Get logged in user profile
       $http.get('/user').then(function (response){
            $scope.user = angular.fromJson(response.data);
  			 	    $scope.userEmail = $scope.user.email;
  			 	    console.log(angular.fromJson(response.data).email);
  			 	  });
        //Create post 
		 	  $http({
            method  : 'POST',
            url     : '/post/' + $scope.userEmail,
            data    : $.param($scope.post, $scope.user), 
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function(response){
           //window.alert($scope.post.text);
            //window.alert(response.body);
            //console.log(response.body);
            console.log($scope.post.text);
            $scope.postCount = $scope.postCount + 1;
            
        });

      };
      
      
  $scope.PostDelete = function(){
        
       //Get logged in user profile
       $http.get('/user').then(function (response){
            $scope.user = angular.fromJson(response.data);
  			 	    $scope.userEmail = $scope.user.email;
  			 	    console.log(angular.fromJson(response.data).email);
  			 	  });
      
        //Delete post       
		 	  $http({
            method  : 'DELETE',
            url     : '/post/' + $scope.userEmail,
            data    : $.param($scope.post, $scope.user), 
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).success(function(response){
            //window.alert($scope.post.text);
            //window.alert(response.body);
            //console.log(response.body);
            console.log($scope.post.text);
            $scope.postCount = $scope.postCount - 1;
        });

      };

  $scope.toggleDropdown = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };

}); 
    
myApp.controller('trackingController', function ($scope, $http) {
    $scope.status = {
    isopen: false
  };

  $scope.postDisplay = function(){
    //Get logged in user profile
     $http.get('/user').then(function (response){
          $scope.user = angular.fromJson(response.data);
			 	    $scope.userEmail = $scope.user.email;
			 	    console.log(angular.fromJson(response.data).email);
			 	  });
    
     $http({
          method: 'PUT',
          url: "/post", 
          data: JSON.stringify({email: $scope.userEmail})}).then(function (response,err) {
                console.log("User email: " + $scope.userEmail);
                
                if(err){
                    console.log(err);
                }else{
                    $scope.postList = $scope.userEmail + " posted: " + JSON.stringify(response.data);
                    //alert(JSON.stringify(response.data));
                }
          });
  };
  
  $scope.toggleDropdown = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };

//   $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
}); 
    
myApp.controller('commentsController', function ($scope, $log) {
    $scope.status = {
    isopen: false
  };

  $scope.toggleDropdown = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.status.isopen = !$scope.status.isopen;
  };

//   $scope.appendToEl = angular.element(document.querySelector('#dropdown-long-content'));
});
    
angular.module('myModule', [])
.controller('viewController', ["$scope", "$http", function($scope, $http){
    // Initial login function
    $scope.isLogin = true;
    
    $scope.login = function(){
	        
	        
	        
	        var client_id="706981438981-40ico2fbi6eqmeqsrhbpgnmi54siblol.apps.googleusercontent.com";
      		var scope="email";
      		var redirect_uri="https://lwei47-ece9065-lab3-lwei47.c9users.io/login";
      		var response_type="token";
      		var state="lwei47";
      		var url="https://accounts.google.com/o/oauth2/auth?scope="+scope+"&client_id="+client_id+"&redirect_uri="+redirect_uri+"&response_type="+response_type+"&state="+state;
      		//window.location.replace(url);
      		
      		w = window.open(url, 'window', 'status=0,menubar=0,resizable=1,width=500,height=800;');
      		
      		
      		$(document).ready(function() { 
      
      			$.get('/login', function(data) {
      				url = data;
      				window.location.replace(url);
      			});
            //window.location = '/#/posting';
      			
        			var interval = window.setInterval((function() {
        			 	if (w.closed) {
        			 	  $scope.userEmail = "Initial";
        			 	  $scope.isLogin = false;
        			 	  
        			 	  $http.get('/user').then(function (response){
        			 	    $scope.userEmail = angular.fromJson(response.data).email;
        			 	    console.log(angular.fromJson(response.data).email);
        			 	  });
        			 		window.clearInterval(interval);
        			 		window.location = '/#/posting';
        			 	  }
        			 }),1000);
      		});

	    
    }
    $scope.logout = function(){
      

      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        this.isLogin = true;
        window.location.replace('https://lwei47-ece9065-lab3-lwei47.c9users.io');
        console.log('User signed out.');
      });
      
      auth2.disconnect();
    }
    
    $scope.onSignIn = function(googleUser) {
            
        var auth2 = gapi.auth2.getAuthInstance();
        console.log('access_toke: ' + googleUser.getAuthResponse().access_token);
        $scope.id_token = googleUser.getAuthResponse().id_token;
        console.log('id_toke: ' + googleUser.getAuthResponse().id_token);
        
        var port = process.env.PORT;
        var ip   = process.env.IP;
        

        
        mongoose.connect("mongodb://localhost:27017/googleUser",function(error,db){
          if(!error){
           console.log("We are connected");
          }
          else{
           console.dir(error); //failed to connect to [127.4.68.129:8080]               
          }
        });
        
        var profile = googleUser.getBasicProfile();
        console.log(profile.getId()); 
        console.log(profile.getName());
        console.log(profile.getImageUrl());
        console.log(profile.getEmail());
    }
    
    
    $scope.postTrack = function(){
        $http.get('/post').success(function(data) {
             $scope.userposts=data; 
             
            });
      window.location.href = '/#/posting';
    }

}])
//-------- create the view module and name-----------//


function start(){
    gapi.load('auth2', function() {
      
      auth2 = gapi.auth2.init({
        client_id: '706981438981-40ico2fbi6eqmeqsrhbpgnmi54siblol.apps.googleusercontent.com',
        cookiepolicy: 'none',
        scope: 'https://www.googleapis.com/auth/plus.login'
      });
    });
}

function signInCallback(authResult) {
                if (authResult['code']) {
              
                  // Hide the sign-in button now that the user is authorized, for example:
                  // $('#signinButton').attr('style', 'display: none');
              
                  // Send the code to the server
                  $.ajax({
                    type: 'POST',
                    url: 'https://lwei47-ece9065-lab3-lwei47.c9users.io/auth/google/callback',
                    contentType: 'application/octet-stream; charset=utf-8',
                    success: function(result) {
                      // Handle or verify the server response.
                    },
                    processData: false,
                    data: authResult['code']
                  });
                } else {
                  
                    console.log('Redirect Error');
                  
                }
              }